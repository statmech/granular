#define POW2(x) (x)*(x)  

/* replica_tag:
 * 0,0 means real particle.
 * -1,0 means the replica which is to the left
 * 1,-1 means the replica which is right and below
 */
struct replica_tag
{
  short x; /* values: -1,0,1 */
  short y; /* values: -1,0,1 */
};
typedef struct replica_tag replica_t;

struct particle_tag
{
  double x;
  double y;
  double vx;
  double vy;
};
typedef struct particle_tag particle_t;
struct collision_tag
{
  double t;       /* time to the collison */
  size_t p1_i; /* index of a particle which is going to collide */
  size_t p2_i; /* index of the other particle (or a replica of it) */ 
  /*
   * Since particles can:
   *   one of the particles which will collide can be a replica
   *   one or both particle can change from real to replica during t
   *   
   * Passing effective particle which was detected to collide will
   * save some calculations
   */
  particle_t p_eff;
};
typedef struct collision_tag collision_t;

/* Critical vars */
extern size_t n_particles_sqrt;
extern size_t n_particles;
extern particle_t * particle_set;
extern collision_t last_coll;
extern double current_time;
extern double radius;
extern double box_side;
extern double epsilon; /* 1 means totally elastic; 0 means totally inelastic */

extern double draw_delay;
extern short is_draw_particles;
extern short is_draw_replicas;
extern short is_draw_blinking_coll;
extern short is_draw_ugly_mode;
extern short refresh_ratio; /* ms */

extern short is_measure;
extern short is_measure_speed_module;
extern short is_measure_speed_x;
extern short is_reset_speed;
extern short is_measure_energy;
extern size_t measure_after_n_steps;
extern size_t measure_n_bins;
extern double measure_extra_bins_factor;

extern short is_plot_measures; 
extern short is_plot_final_energy;
extern size_t plot_between_n_steps;
