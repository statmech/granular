#include <stdlib.h>
#include <gsl/gsl_histogram.h>

#include "defs.h"
#include "dynamic.h"
#include "measuring.h"

#define r_filename  "distances.data"
#define sm_filename "speed_module.data"
#define sx_filename "speed_x.data"
#define e_filename  "energy.data"

FILE *
setup_gnuplot (void)
{
  FILE * p;
  p = popen ("gnuplot", "w");
  if (p == NULL)
    {
      perror ("popen");
      exit (1);
    }
  /*  fputs ("unset key\n", p);*/
  fputs ("f_vx(x)=1/sqrt(pi*vmean2)*exp(-x**2/vmean2)\n", p);
  fputs ("f_v(x)=2*x/vmean2*exp(-x**2/vmean2)\n", p);
 return p;
}

static void
write_distance_data (gsl_histogram * h_r_norm)
{
  FILE *r_file;
  r_file = fopen (r_filename, "w");
  gsl_histogram_fprintf (r_file, h_r_norm, "%g", "%g");
  fclose (r_file);
}

static void
write_speed_data (gsl_histogram * h_sm_norm, gsl_histogram * h_sx_norm)
{
  FILE *sm_file, *sx_file;
  sm_file = fopen (sm_filename, "w");
  gsl_histogram_fprintf (sm_file, h_sm_norm, "%g", "%g");
  fclose (sm_file);

  sx_file = fopen (sx_filename, "w");
  gsl_histogram_fprintf (sx_file, h_sx_norm, "%g", "%g");
  fclose (sm_file);
}

static void
write_energy_data (FILE * e_file, double t, double energy)
{
  fprintf (e_file, "%.15g %.15g\n", t, energy);
  fflush (e_file);
}

static void
plot_data_files (FILE * p, double e_norm)
{
  /* 4 graphs */
  fputs ("set multiplot layout 2,2\n", p);
  /* Set average energy */
  fprintf (p, "vmean2=%.15g\n", e_norm);
  /* Plot speed module */
  fprintf (p, "set title \"Start measuring: %d; Plot: 1/%d\"\n",
	   measure_after_n_steps, plot_between_n_steps);

  fprintf (p, "set xlabel \"v\"\n");
  fprintf (p, "set ylabel \"P(v)\"\n");
  fprintf (p, "set xrange [0:*]\n");  
  fputs ("plot \"", p);
  fputs (sm_filename, p);
  fprintf (p, "\" u 1:3 w l t \"%d-particles sys.\"", n_particles);
  fprintf (p, ", f_v(x) t \"<E>=%f\"\n", e_norm);
  /* Plot speed x */

  fprintf (p, "set xlabel \"vx\"\n");
  fprintf (p, "set ylabel \"P(vx)\"\n");
  fprintf (p, "set xrange [*:*]\n");  
  fputs ("plot \"", p);
  fputs (sx_filename, p);
  fputs ("\" u 1:3 w l not", p);
  fputs (", f_vx(x) not\n", p);

  /* Plot energy */
  fprintf (p, "set xlabel \"t\"\n");
  fprintf (p, "set ylabel \"<E>\"\n");
  fprintf (p, "unset title\n");  
  fprintf (p, "set xrange [*:*]\n");  
  fprintf (p, "set logscale\n");  
  fputs ("plot \"", p);
  fputs (e_filename, p);
  fputs ("\" w l not", p);
  fputs (", 1000*x**(-2) t \"t**-2\"\n", p);
  fputs ("unset logscale\n", p);

  /* Plot distances */
  fprintf (p, "set xlabel \"r\"\n");
  fprintf (p, "set ylabel \"P(r)\"\n");
  fprintf (p, "set xrange [0:*]\n");  
  fputs ("plot \"", p);
  fputs (r_filename, p);
  fputs ("\" u 1:3 w l not\n", p);

  /* It's really important to flush if you want to see the graphs soon */
  fflush(p);
}

void
plot_measures (FILE * p, FILE * e_file,
	       gsl_histogram * h_r_norm, gsl_histogram * h_sm_norm, gsl_histogram * h_sx_norm,
	       double e_norm)
{
  /* Create files with pdf's and plot */
  write_distance_data (h_r_norm);
  write_speed_data (h_sm_norm, h_sx_norm);
  write_energy_data (e_file, current_time, e_norm);
  plot_data_files (p, e_norm);
}

void
measure_and_plot (size_t do_n_measures,
		  gsl_histogram * h_r, gsl_histogram * h_sm, gsl_histogram * h_sx,
		  double w_r, double w_sm, double w_sx, size_t measure_n_bins,
		  size_t n_measures_done, FILE * p, FILE * e_file)
{
  /* Define vars to normalize the measures */
  gsl_histogram * h_r_norm;
  gsl_histogram * h_sm_norm;
  gsl_histogram * h_sx_norm;
  double scale_r;
  double scale_sm;
  double scale_sx;
  double e_norm;

  /* Evolve and measure the system */
  if (is_plot_final_energy)
    {
      take_long_measure (do_n_measures-1, h_r, h_sm, h_sx);
      e_norm = take_long_measure (1, h_r, h_sm, h_sx);
    }
  else
    e_norm = take_long_measure (do_n_measures, h_r, h_sm, h_sx);

  /* Normalize results for plotting */
  e_norm = e_norm/n_particles;
  h_r_norm = gsl_histogram_clone (h_r);
  h_sm_norm = gsl_histogram_clone (h_sm);
  h_sx_norm = gsl_histogram_clone (h_sx);
  scale_r  = 1/((n_measures_done + do_n_measures)*w_r* n_particles);
  scale_sm = 1/((n_measures_done + do_n_measures)*w_sm*n_particles);
  scale_sx = 1/((n_measures_done + do_n_measures)*w_sx*n_particles);
  gsl_histogram_scale (h_r_norm, scale_r);
  gsl_histogram_scale (h_sm_norm, scale_sm);
  gsl_histogram_scale (h_sx_norm, scale_sx);

  /* Plot */
  plot_measures (p, e_file, h_r_norm, h_sm_norm, h_sx_norm, e_norm);
  /* Free the histograms */
  gsl_histogram_free (h_sm_norm);
  gsl_histogram_free (h_sx_norm);
}


/* 
 * This function is intended to execute the program without drawing
 * but measuring observables.
 */
void
launch_plot_loop ()
{
  size_t n_measures_done;
  /* Open a pipe to gnuplot */
  FILE * p = setup_gnuplot ();
  FILE * e_file = fopen (e_filename, "w");

  /* Allocate some histograms */
  gsl_histogram * h_r  = gsl_histogram_alloc (3*measure_n_bins); /*distances*/
  gsl_histogram * h_sm = gsl_histogram_alloc (measure_n_bins); /*speed module*/
  gsl_histogram * h_sx = gsl_histogram_alloc (measure_n_bins); /*speed x*/
  double w_r, w_sm, w_sx;

  /* Set histograms according to state of the system */
  w_sm = adjust_histogram_sm (h_sm, measure_n_bins);
  w_sx = adjust_histogram_sx (h_sx, measure_n_bins);
  gsl_histogram_set_ranges_uniform (h_r, 0., box_side);
  w_r  = box_side/(3*measure_n_bins);

  /* Step until the moment of start measuring */
  run_n_steps (measure_after_n_steps);

  n_measures_done = 0;
  do
    {
      /* Measure and plot all the time */
      measure_and_plot (plot_between_n_steps,
			h_r, h_sm, h_sx, w_r, w_sm, w_sx, measure_n_bins,
			n_measures_done, p, e_file);
      if (is_reset_speed)
	{
	  /* Set new range for histograms */
	  gsl_histogram_reset (h_r);
	  w_sm = adjust_histogram_sm (h_sm, measure_n_bins);
	  w_sx = adjust_histogram_sx (h_sx, measure_n_bins);
	}
      else
	n_measures_done += plot_between_n_steps;
    }
  while (1);
}

