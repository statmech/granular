#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "defs.h"
#include "collision.h"

/* Particle MUST end into the box, use equallity comparison accordly*/
static double
correct_coordinate (double z)
{
  if (z > box_side) return z - box_side;
  else
  {
    if (z >= 0.) return z;
    else return z + box_side;
  }
}

void
move_particles (double t)
{
  size_t new_p1_i;

  /*
   * Move all particles
   */
  for (new_p1_i = 0; new_p1_i < n_particles; new_p1_i++)
    {
      particle_set[new_p1_i].x =
	correct_coordinate (particle_set[new_p1_i].x
			   + particle_set[new_p1_i].vx * t);
      particle_set[new_p1_i].y =
	correct_coordinate (particle_set[new_p1_i].y
			    + particle_set[new_p1_i].vy * t);
    }
}

void
change_speed_of_colliding_particles (void)
{
  double elastic_correction = (1.+epsilon)/2.;
  /* Normalize relative position vector (k) */  
  double rnorm = sqrt(POW2(last_coll.p_eff.x) + POW2(last_coll.p_eff.y));
  /* Position arguments of pdiff are now a unitary vector from p2 to p1*/
  double kx = last_coll.p_eff.x /= rnorm;
  double ky = last_coll.p_eff.y /= rnorm;
  /* Collision or transversal component of the speed */
  double kdotv =
    last_coll.p_eff.x*last_coll.p_eff.vx + last_coll.p_eff.y*last_coll.p_eff.vy;

  /*
    printf ("Particle %d and %d are colliding.\n",
    last_coll.p1_i, last_coll.p2_i);
    printf ("Original pos=(%f,%f); speed=(%f,%f)\n\n",
    particle_set[last_coll.p1_i].x,
    particle_set[last_coll.p1_i].y, particle_set[last_coll.p2_i].x,
    particle_set[last_coll.p2_i].y);
    printf ("Time needed until the collision:%f\n", last_coll.t);
  */
  /*
   * Change speed:
   *   Reverse speed direction
   *   Distribute equally (1/2) of total speed to each particle
   *   But measure with the elasticity parameter
   */
  particle_set[last_coll.p1_i].vx -= elastic_correction * kdotv * kx;
  particle_set[last_coll.p1_i].vy -= elastic_correction * kdotv * ky;
  particle_set[last_coll.p2_i].vx += elastic_correction * kdotv * kx;
  particle_set[last_coll.p2_i].vy += elastic_correction * kdotv * ky;  
}

/* For avoiding problems, use this function as the lower level one
 * for stepping.
 * It sets extern variable current time.
 */

void
step_dynamic (void)
{
  /* Found next collision: when and who (info saved on last_coll)*/
  find_out_collision ();
  /* Move all the particles until that moment */
  move_particles (last_coll.t);
  /* Apply effects of the collision */
  change_speed_of_colliding_particles ();
  /* Set current time*/
  current_time += last_coll.t;
}

void
run_n_steps (size_t n_steps)
{
  size_t step;
  for (step = 0; step < n_steps; step++)      
    step_dynamic ();
}

/* WARNING: Run by first time after an individual find_out_collision */
void
run_during (double t_delta)
{
  double t2stop = t_delta;
  /* Found next collision: when and who (info saved on last_coll)*/
  while (last_coll.t < t2stop)
    {
      /* Move all the particles until collision */
      move_particles (last_coll.t); 
      /* Update times */
      current_time += last_coll.t;
      t2stop -= last_coll.t;
      /* Apply effects of the collision */
      change_speed_of_colliding_particles ();      
      /* Calclulate next collision */
      find_out_collision ();
    }
  move_particles (t2stop);
  current_time += t2stop;
  /* Correct time until next collision */
  last_coll.t -= t2stop;
}
