#include <stdio.h>
#include <stdlib.h>
#include <float.h> /* for DBL_MAX macro */
#include <math.h>

#include "defs.h"

/*
 * v_coll stands for (time until) virtual collision.
 *
 * it is updated always that a sooner collision is found. 
 * last value of v_coll will be the real collision.
 *
 * the rest of these vars are used to simplificate access between
 * different functions
 */
collision_t v_coll;
size_t new_p1_i, new_p2_i;
particle_t p_eff;

/*
 * It sets new_t to -1.0 if collision is impossible.
 * Otherwise, it sets new_t to the time of collision.
 *
 * For calculations, it uses the effective values
 * saved in global var p_eff.
 */
double
set_collision_time (particle_t p)
{
  double rdotv = p.x*p.vx + p.y*p.vy;

  if (rdotv > 0.) return -1.0; /* space between particles is increasing */
  else
    {
      double sumvpow2 = POW2(p.vx) + POW2(p.vy);
      double dpow2    = POW2(p.x)  + POW2(p.y) - 4*POW2(radius);
      double discr    = POW2(rdotv) - sumvpow2*dpow2; 
      /* Check you have no collapsed particles */
      if (dpow2 < 0.)
	{
	  printf ("Particles are collapsing! %d and %d\n", new_p1_i, new_p2_i);
	  system ("sleep 30");
	  exit (1);
	}
      if (dpow2 == 0.) printf ("Some particles are touching themselves!");
      /* discr <= 0 means no collision */
      return discr < 0. ? -1.0 : -(rdotv + sqrt(discr))/sumvpow2;
    }
}


/* p1 is changed such a way that we move to the PV of p2 */
particle_t
reduce_p (particle_t p1, particle_t p2)
{
  particle_t p;
  p.x  = p1.x  - p2.x;
  p.y  = p1.y  - p2.y;
  p.vx = p1.vx - p2.vx;
  p.vy = p1.vy - p2.vy;
  return p;
}

/*
 * Which is p_eff when replacing p2 by one of its replicas?
 */
particle_t
p_eff_replica (replica_t rep)
{
  particle_t p;
  /* p_eff = p1 - p2, so when you increase p2.i, p_eff.i is decreasing */
  p.x  = p_eff.x - rep.x*box_side;
  p.y  = p_eff.y - rep.y*box_side;
  p.vx = p_eff.vx;
  p.vy = p_eff.vy;
  
  return p;
}

/*
 * Check collision of p1 with replica rep of p2 (through var p_eff)
 *   if collision: return 1
 *   else:         return 0
 */

int
check_replica (replica_t rep)
{
  particle_t p = p_eff_replica (rep);
  double new_t = set_collision_time (p);
  if (new_t > 0.)
    {
      if (new_t == v_coll.t) printf ("Two collisions at the same time! Problems!!\n");
      if (new_t < v_coll.t)
	{
	  v_coll.t     = new_t;
	  v_coll.p1_i  = new_p1_i;
	  v_coll.p2_i  = new_p2_i;
	  v_coll.p_eff = p;
	}
      return 1;
    }
  else return 0;
}

/*
 * This function checks for collision between p1 and 3 replicas of p2
 * in the quadrant passed as an argument. *Only quadrant values showed in
 * picture of below are expected*
 *
 * If some collision is found, the others are not checked
 * (since they are impossible: it is supposed p1 is inside
 * the quadrant passed as argument and box_side is large enough).
 *
 *    {-1,+1}   {0,+1}    {+1,+1}
 *       o--------o--------o 
 *       |        |        |
 *       |(-1,+1) |(+1,+1) |          (,) quadrant
 *       |        |        |          {,} replica
 *       |        |p2      |           
 * {-1,0}o--------o--------o {+1,0}      .
 *       |        |        |            /|\
 *       |(-1,-1) |(+1,-1) |             | box_side
 *       |        |        |             |
 *       |        |        |            \|/
 *       o--------o--------o             . 
 *    {-1,-1}   {0,-1}    {+1,-1}
 *
 */

void
check_quadrant (short x_dir, short y_dir)
{
  replica_t rep;

  rep.x = x_dir;
  rep.y = 0;
  if (! check_replica (rep))
    {
      rep.x = 0;
      rep.y = y_dir;
      if (! check_replica (rep))
	{
	  rep.x = x_dir;
	  rep.y = y_dir;
	  check_replica (rep);
	}
    }
}


void
find_out_in_all_replicas (void)
{
  replica_t rep;
  rep.x = +1; rep.y =  0; check_replica (rep);
  rep.x = +1; rep.y = +1; check_replica (rep);
  rep.x =  0; rep.y = +1; check_replica (rep);
  rep.x = -1; rep.y = +1; check_replica (rep);
  rep.x = -1; rep.y =  0; check_replica (rep);
  rep.x = -1; rep.y = -1; check_replica (rep);
  rep.x =  0; rep.y = -1; check_replica (rep);
  rep.x = +1; rep.y = -1; check_replica (rep);
}

void
find_out_replicas (void)
{
  /*
   * If (p_eff.x == 0 || p_eff.y == 0) you can check only one replica
   * since you are more than box_side from others. && is impossible since p1 is there.
   */
  if      (p_eff.x > 0. && p_eff.y > 0.) check_quadrant (+1,+1);
  else if (p_eff.x > 0. && p_eff.y < 0.) check_quadrant (+1,-1);
  else if (p_eff.x < 0. && p_eff.y > 0.) check_quadrant (-1,+1);
  else if (p_eff.x < 0. && p_eff.y < 0.) check_quadrant (-1,-1);
  /* From here, p_eff.x == 0 || p_eff.y == 0 */
  else if (p_eff.y > 0.) {replica_t r; r.x =  0; r.y = +1; check_replica (r);}      
  else if (p_eff.y < 0.) {replica_t r; r.x =  0; r.y = -1; check_replica (r);}      
  else if (p_eff.x > 0.) {replica_t r; r.x = +1; r.y =  0; check_replica (r);}      
  else if (p_eff.x < 0.) {replica_t r; r.x = -1; r.y =  0; check_replica (r);}      
  else printf ("We are having problems determining position of replicas!\n");
}

/*
 * It is the only accesible function from outside this file.
 * 
 * Side effects:
 * If no collision is posible between any two particles
 * nor between any particle and all its 3 nearest replicas
 * relative to rest of particles, program exits.
 * 
 * Otherwise:
 *   extern variable coll is set with the sooner collision.
 */

void
find_out_collision (void)
{
  /* max init value, so first check will match as better collision */
  v_coll.t = DBL_MAX;

  /* loop over all different pair of particles (triangulary) */
  for (new_p1_i = 0; new_p1_i < n_particles; new_p1_i++)
    {
      for (new_p2_i = new_p1_i+1; new_p2_i < n_particles; new_p2_i++)
	{
	  /* Avoid checking pair of last collision: they are touching themselves */
	  /* But one of them really can be the next one, so use || instead of &&  */
	  if (new_p2_i != last_coll.p2_i || new_p1_i != last_coll.p1_i)
	    {
	      short is_real_collision;
	      replica_t rep;
	      rep.x = 0;
	      rep.y = 0;

	      /* Set effective particle of the two being dealed */
	      p_eff = reduce_p (particle_set[new_p1_i], particle_set[new_p2_i]);
	      /* check if particles collide between them */
	      is_real_collision = check_replica (rep);
	      /* else, check replicas */
	      if (! is_real_collision)
		{
		  /*
		   * Facts:
		   *   * p1 can only touch one particle of p2 or the 3 nearest replicas
		   *     (considering also real p2) assured box_size is large enough.
		   *   * any "collision" with further replicas would happen later than
		   *     hipotetical "collision" with 3 nearest or real one.
		   *   * if no collision is found for those 4, you could think in
		   *     considering other replicas (changing source code),
		   *     but probably would be better to increase density of particles
		   *     in the box.
		   */

		  /* Check 3 nearest p2 replicas of p1.*/
		  find_out_replicas ();
		  /* 
		   * No need of checking "collisions of p2 with replicas of p1".
		   * Those results are simetric. 
		   */
		}
	    }
	}
    }
  last_coll = v_coll;
}
