size_t n_particles_sqrt;
size_t n_particles;
particle_t * particle_set;
collision_t last_coll;
double current_time;
double radius;
double box_side;
double epsilon;

double draw_delay;
short is_draw_particles;
short is_draw_replicas;
short is_draw_blinking_coll;
short is_draw_ugly_mode;
short refresh_ratio;

short is_measure;
short is_measure_speed_module;
short is_measure_speed_x;
short is_measure_energy;
short is_reset_speed;
size_t measure_after_n_steps;
size_t measure_n_bins;
double measure_extra_bins_factor;

short is_plot_measures;
short is_plot_final_energy;
size_t plot_between_n_steps;
