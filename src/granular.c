#include <stdio.h>
#include <time.h>
#include <stdlib.h> /* for exit and malloc */
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_histogram.h>
#include <gtk/gtk.h>

#include "defs.h"
#include "extern.h"
#include "collision.h"
#include "dynamic.h"
#include "measuring.h"
#include "drawing.h"
#include "plotting.h"

static void
init_global_vars (void)
{
  /* Parameters */
  double spacing = 5.0;
  n_particles_sqrt = 15;
  n_particles = n_particles_sqrt*n_particles_sqrt;
  radius = 1.5;
  epsilon = 0.3;

  box_side = n_particles_sqrt*2*radius*spacing;

  /* Drawing options */
  is_draw_particles = 1; /* Active drawing */
  draw_delay = 10.e4;
  is_draw_replicas = 0;
  is_draw_blinking_coll = 0;
  is_draw_ugly_mode = 0;
  refresh_ratio = 100; /* ms */

  /* Measuring options */
  is_measure_speed_module = 1;
  is_measure_speed_x = 1;
  is_reset_speed = 1;
  is_measure_energy = 1;
  measure_after_n_steps = 1;
  measure_n_bins = 40;
  measure_extra_bins_factor = 1.5;
  /* Plotting options */
  is_plot_measures = 1;
  is_plot_final_energy = 1;
  plot_between_n_steps = 500;
}


static void
init_particle_positions (void)
{
  size_t i, j, k;
  double xpos, ypos;
  /* inital separation between centers of particles*/
  double space = box_side/n_particles_sqrt;
  if (space < 2.0*radius)
    {
      printf ("Too much particles!\n");
      exit(1);
    }
  
  k = 0;
  xpos = ypos = space/2.;
  for (i=0; i<n_particles_sqrt; i++)
    {
      for (j=0; j<n_particles_sqrt; j++)
	{
	  particle_set[k].x  = xpos;
	  particle_set[k].y  = ypos;
	  /* intial speed as a normal distribution */

	  xpos += space;
	  k++;
	}
      xpos = space/2.;
      ypos += space;
    }
}

static void
init_particle_speeds (void)
{
  size_t k;
  double vx_total, vy_total;
  double vx_correction, vy_correction;
  const gsl_rng_type * T;
  gsl_rng * r;

  /* Start random number generator */
  gsl_rng_env_setup();
  gsl_rng_default_seed = time(0);
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  vx_total = vy_total = 0.;
  for (k=0; k<n_particles; k++)
    {
      particle_set[k].vx = gsl_ran_gaussian (r, 1.0);
      particle_set[k].vy = gsl_ran_gaussian (r, 1.0);
      vx_total += particle_set[k].vx;
      vy_total += particle_set[k].vy;
    }

  gsl_rng_free (r);

  /* Correct speed of every particle so total speed is 0*/
  vx_correction = vx_total/n_particles;
  vy_correction = vy_total/n_particles;
  for (k=0; k<n_particles; k++)
    {
      particle_set[k].vx -= vx_correction;
      particle_set[k].vy -= vy_correction;
    }
}

/* Initialize the set of particles */
static void
init_particle_set (void)
{

  /* Allocate the particles */
  particle_set = malloc (n_particles * (sizeof (particle_t)));
  if (particle_set == NULL) exit(1);   

  init_particle_positions ();
  init_particle_speeds ();
}


int
main (int argc, char *argv[])
{
  init_global_vars ();
  init_particle_set ();

  if (is_draw_particles)
    {
      gtk_init (&argc, &argv);
      /* Find first collision before entering gtk_loop */
      find_out_collision ();
      launch_gtk_loop ();
    }
  else
    {
      if (is_plot_measures)
	/* Find first collision before entering measure_loop */
	find_out_collision ();
	launch_plot_loop ();
    }
  return 0;
}

