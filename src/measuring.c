#include <stdio.h>
#include <math.h>
#include <gsl/gsl_histogram.h>

#include "defs.h"
#include "dynamic.h"
#include "collision.h"

static double
module2 (double x, double y)
{
  return x*x + y*y;
}

double
get_max_speed_module2 (void)
{
  size_t p_i;
  double max;
  max = 0.;
  for (p_i = 0; p_i < n_particles; p_i++)
    {
      double v_mod2 = module2(particle_set[p_i].vx, particle_set[p_i].vy);
      if (max < v_mod2) max = v_mod2;
    }
  return max;
}

double
get_max_speed_module (void)
{
  return (sqrt (get_max_speed_module2 ()));
}

double
get_max_abs_speed_x (void)
{
  size_t p_i;
  double max;

  max = 0.;
  for (p_i = 0; p_i < n_particles; p_i++)
    {
      double v = fabs (particle_set[p_i].vx);
      if (max < v) max = v;
    }
  return max;
}

/*
 * It returns total energy of the current system and increase histograms
 * of the pdf of speed module and speed x passed as arguments.
 * Energy is kinetic: 1/2 * m * v**2 = v**2 (let's consider m=2)
 */

double
take_instant_measure (gsl_histogram * h_r, gsl_histogram * h_sm, gsl_histogram * h_sx)
{
  size_t p_i, p_j;
  double total_energy;
  total_energy = 0.;
  for (p_i = 0; p_i < n_particles; p_i++)
    {
      double v2 = module2 (particle_set[p_i].vx, particle_set[p_i].vy);
      total_energy += v2;
      if (is_measure_speed_module)
	gsl_histogram_increment (h_sm, sqrt(v2));
      if (is_measure_speed_x)
	gsl_histogram_increment (h_sx, particle_set[p_i].vx);
      for (p_j = p_i+1; p_j < n_particles; p_j++)
	{
	  /* Avoid just the pair colliding (d=2r) */
	  if (p_i != last_coll.p1_i || p_j != last_coll.p2_i)
	    {
	      particle_t p = reduce_p (particle_set[p_i], particle_set[p_j]);
	      gsl_histogram_increment (h_r, sqrt(module2(p.x, p.y)));
	    }
	}
    }
  return total_energy;
}

/* Acumulate statistic of speed during n_measures and returns average energy */
double
take_long_measure (size_t n_measures,
		   gsl_histogram * h_r, gsl_histogram * h_sm, gsl_histogram * h_sx)
{
  double energy;
  size_t step;
  energy = 0.;
  for (step = 0; step < n_measures; step++) 
    {
      /* Acummulate speed statistics; drop energy */
      energy += take_instant_measure (h_r, h_sm, h_sx);
      step_dynamic ();
    }
  return energy/n_measures;
}

double
adjust_histogram_sm (gsl_histogram * h_sm, size_t n_bins)
{
  double max_sm = measure_extra_bins_factor*(get_max_speed_module ());
  double w_sm = max_sm/n_bins;
  gsl_histogram_set_ranges_uniform (h_sm, 0., max_sm);
  return w_sm;
}

double
adjust_histogram_sx (gsl_histogram * h_sx, size_t n_bins)
{
  double max_sx = measure_extra_bins_factor*(get_max_abs_speed_x ());
  double w_sx = 2*max_sx/measure_n_bins;
  gsl_histogram_set_ranges_uniform (h_sx, -max_sx, max_sx);
  return w_sx;
}
