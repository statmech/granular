#if GTK_MAJOR_VERSION == 3
#define CORRECT_DRAW_SIGNAL "draw"
#endif
#if GTK_MAJOR_VERSION == 2
#define CORRECT_DRAW_SIGNAL "expose_event"
#endif

#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include <gsl/gsl_histogram.h>

#include "defs.h"
#include "dynamic.h"
#include "plotting.h"

#define sm_filename "speed_module.data"
#define sx_filename "speed_x.data"
#define e_filename  "energy.data"


static void
draw_speed (cairo_t *cr, double x, double y, double vx, double vy)
{ 
  cairo_move_to(cr, x, y);
  cairo_line_to(cr, x+vx, y+vy);
  cairo_stroke(cr);    
}

static void
draw_particle (cairo_t *cr, double x, double y, double r)
{ 
  cairo_arc(cr, x, y, r, 0, 2 * M_PI);
  if (is_draw_ugly_mode) cairo_stroke(cr);
  else cairo_fill(cr);
}

static void
blink_collision (cairo_t *cr, double scale)
{
  cairo_set_source_rgb(cr, 1., 0., 0.);  
  draw_particle (cr, scale*particle_set[last_coll.p1_i].x,
		 scale*particle_set[last_coll.p1_i].y,
		 scale*radius);
  draw_particle (cr, scale*particle_set[last_coll.p2_i].x,
		 scale*particle_set[last_coll.p2_i].y,
		 scale*radius);
}

static void
draw_all_particles (cairo_t *cr, double offset_x, double offset_y, double scale)
{ 
  int p_i;
  cairo_set_line_width(cr, 1.);
  for (p_i = 0; p_i < n_particles; p_i++)
    {
      if (is_draw_ugly_mode) cairo_set_source_rgb(cr, 0., 0., 0.);
      else
	{
	  /* I want a map from (0, n_particles-1) to different colors */
	  double d_color = p_i/ (double) (n_particles - 1);
	  cairo_set_source_rgb(cr, d_color, 1. - d_color, 0.5);
	}
      /* Draw particles */
      draw_particle (cr, offset_x + scale*particle_set[p_i].x,
		     offset_y + scale*particle_set[p_i].y,
		     scale*radius);
      /* Draw speeds as a bar */
      cairo_set_source_rgb(cr, 0., 0., 0.);  
      draw_speed (cr, offset_x + scale*particle_set[p_i].x,
		  offset_y + scale*particle_set[p_i].y,
		  scale*particle_set[p_i].vx,
		  scale*particle_set[p_i].vy);
    }

  if (is_draw_blinking_coll) blink_collision (cr, scale);
}

static void
draw_replicas_limit (cairo_t *cr, int side)
{
  cairo_set_line_width(cr, 0.5);
  cairo_set_source_rgb(cr, 1., 0., 0.);

  cairo_set_source_rgb(cr, 0., 0., 0.);  
  cairo_move_to(cr, 0., side/3);
  cairo_line_to(cr, side, side/3);
  cairo_stroke(cr);
  cairo_move_to(cr, 0., 2*side/3);
  cairo_line_to(cr, side, 2*side/3);
  cairo_stroke(cr);
  cairo_move_to(cr, side/3., 0.);
  cairo_line_to(cr, side/3., side);
  cairo_stroke(cr);
  cairo_move_to(cr, 2*side/3., 0.);
  cairo_line_to(cr, 2*side/3., side);
  cairo_stroke(cr);
}

static void
draw_all_particles_and_replicas (cairo_t *cr, double replica_width, double scale)
{ 
  /* 1:1 aspect_relation is assured */
  short i, j;

  cairo_set_line_width(cr, 1.);
  for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
	draw_all_particles (cr, i*replica_width, j*replica_width, scale);
}


static gboolean
on_draw_event (GtkWidget *widget, cairo_t *cr)
{      
  gint width, height;
  GtkWidget *window = gtk_widget_get_toplevel(widget); 

  /* get size of the window: it could be changing by the user */
  gtk_window_get_size(GTK_WINDOW(window), &width, &height);
  cr = gdk_cairo_create(gtk_widget_get_window(widget));

  /* draw state of the system */
  if (is_draw_replicas)
    {
      /* 1:1 aspect_relation is assured */
      double scale = width/(3*box_side); 
      draw_all_particles_and_replicas (cr, width/3., scale);
      draw_replicas_limit (cr, width);
    }
  else
    {
      double scale = width/box_side; 
      draw_all_particles(cr, 0., 0., scale);
    }
  cairo_destroy(cr);
  return FALSE;
}

static gboolean
time_handler(GtkWidget *widget)
{

  /*  step_dynamic ();*/
  run_during (draw_delay);

  /* Redraw */
  gtk_widget_queue_draw(widget);
  return TRUE;
}

int
launch_gtk_loop (int * argc, char * argv[])
{
  GtkWidget *window;
  GtkWidget *drawing_area;
  GdkGeometry geometry;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  drawing_area = gtk_drawing_area_new ();
  gtk_container_add (GTK_CONTAINER (window), drawing_area);

  g_signal_connect (G_OBJECT (drawing_area), CORRECT_DRAW_SIGNAL, 
		    G_CALLBACK (on_draw_event), NULL);
  g_signal_connect (G_OBJECT (window), "destroy",
		    G_CALLBACK (gtk_main_quit), NULL);

  /* Window properties */
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_title(GTK_WINDOW(window), "Granular system");

  /* Set aspect ratio*/
  geometry.min_aspect = 1.0;
  geometry.max_aspect = 1.0;
  gtk_window_set_geometry_hints (GTK_WINDOW(window), drawing_area,
  				 &geometry, GDK_HINT_ASPECT);

  /* g_timeout_add_seconds (1, (GSourceFunc) time_handler, (gpointer) window);*/
  g_timeout_add (refresh_ratio, (GSourceFunc) time_handler, (gpointer) window);  

  gtk_widget_show_all (window);

  gtk_main();
  return 0;
}
