double get_max_speed_module2 (void);
double get_max_speed_module (void);
double get_max_abs_speed_x (void);
double take_instant_measure (gsl_histogram *, gsl_histogram *, gsl_histogram *);
double take_long_measure (size_t, gsl_histogram *, gsl_histogram *, gsl_histogram *);
double adjust_histogram_sm (gsl_histogram *, size_t);
double adjust_histogram_sx (gsl_histogram *, size_t);
